# 1st-project

Le but est d'installer et de faire fonctionner un service ansible dans un docker les deux créés par un dockerfile
Le fichier .gitlab-ci.yml permet de lancer les tâches de lint (vérification du code), puis de build (création de l'image docker contenant ansible),
puis le scan puis permet de vérifier des vulnérabilités de sécurité dans l'image ou les paquets utilisés dans l'image.

les tâches de lint, build et de scan sont exécutés sur les serveurs gitlab (appellés "runners") utilisant des technologies docker.




**Authors:**
Jacques Dai
Christophe Ribailly
