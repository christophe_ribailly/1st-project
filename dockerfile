FROM debian:jessie

RUN apt-get update && apt-get install -y git=2.20.1 --no-install-recommends && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y ansible=2.7.7+dfsg-1 --no-install-recommends && apt-get clean && rm -rf /var/lib/apt/lists/*